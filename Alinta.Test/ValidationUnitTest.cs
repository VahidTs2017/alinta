using Alinta.Application.Model;
using Alinta.Application.Validation;
using System;
using Xunit;

namespace Alinta.Test
{
    public class CustomerUnitTest
    {
        public CustomerDto TestModelCreator()
        {
            return new CustomerDto
            {
                DateOfBirth = new DateTime(1990,1,1),
                FirstName = "Vahid",
                LastName = "Salari"
            };
        }


        [Fact]
        public void FirstNameNotNull()
        {

            var model = TestModelCreator();
            model.FirstName = null;
            var validator = new CustomerValidator();
            var validationResult = validator.Validate(model);
            Assert.False(validationResult.IsValid);
        }

        [Fact]
        public void LastNameNotNull()
        {
            var model = TestModelCreator();
            model.LastName = null;
            var validator = new CustomerValidator();
            var validationResult = validator.Validate(model);
            Assert.False(validationResult.IsValid);
        }

        [Fact]
        public void FirstNameCheckLength()
        {
            var model = TestModelCreator();
            model.FirstName = "VahidVahidVahidVahidVahidVahidVahidVahidVahidVahidVahidVahid";
            var validator = new CustomerValidator();
            var validationResult = validator.Validate(model);
            Assert.False(validationResult.IsValid);
        }

        [Fact]
        public void LastNameCheckLength()
        {
            var model = TestModelCreator();
            model.LastName = "VahidVahidVahidVahidVahidVahidVahidVahidVahidVahidVahidVahid";
            var validator = new CustomerValidator();
            var validationResult = validator.Validate(model);
            Assert.False(validationResult.IsValid);
        }

        [Fact]
        public void CheckAbove18()
        {
            var model = TestModelCreator();
            model.DateOfBirth = new DateTime(2010,1,1);
            var validator = new CustomerValidator();
            var validationResult = validator.Validate(model);
            Assert.False(validationResult.IsValid);
        }
    }
}
