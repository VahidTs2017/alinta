﻿namespace Alinta.Application
{
    using Alinta.Application.Model;
    using Alinta.Core.Models;
    using System;
    using System.Threading.Tasks;
    public interface ICustomerApplication
    {
        Task<ApiResponse> AddCustomer(CustomerDto customer);
        Task<ApiResponse> EditCustomer(Guid customerID, CustomerDto customer);
        Task<ApiResponse> DeleteCustomer(Guid customerId);
        Task<ApiResponse> SearchCustomer(string name);
    }
}