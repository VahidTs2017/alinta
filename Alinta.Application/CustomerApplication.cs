﻿namespace Alinta.Application
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using Alinta.Application.Service;
    using Alinta.Application.Model;
    using Alinta.Application.Validation;
    using Alinta.Core.Models;
    using Microsoft.Extensions.Logging;

    public class CustomerApplication : ICustomerApplication
    {
        private readonly ICustomerService _customerService;
        private readonly ILogger<CustomerApplication> logger;
        public CustomerApplication(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public async Task<ApiResponse> AddCustomer(CustomerDto customer)
        {
            var result = new ApiResponse();
            if (customer != null)
            {
                try
                {
                    var errors = "";
                    StringBuilder builder = new StringBuilder();
                    var validator = new CustomerValidator();
                    var validationResult = validator.Validate(customer);
                    if (validationResult.IsValid)
                    {
                        var ret = await _customerService.AddCustomer(customer);
                        result.Status = System.Net.HttpStatusCode.OK;
                        result.Message = "Customer Added";
                    }
                    else
                    {
                        foreach (var msg in validationResult.Errors)
                        {
                           errors= builder.Append(msg).AppendLine().ToString();
                        }
                        result.Message = errors;
                        result.Status = System.Net.HttpStatusCode.BadRequest;
                        result.Result = null;
                        logger.LogError($"Validation Error:{errors}");
                    }
                    return result;
                }
                catch (Exception e)
                {
                    logger.LogError(e.Message);
                    result.Message = "Create customer Failed, Something went wrong!, Contact the Administrator.";
                    result.Status = System.Net.HttpStatusCode.BadRequest;
                    return result;
                }
            }
            else
            {
                result.Message = "customer Is null";
                logger.LogError("customer Is null");
                result.Status = System.Net.HttpStatusCode.InternalServerError;
                result.Result = null;
                return result;
            }
        }

     

        public async Task<ApiResponse> DeleteCustomer(Guid customerId)
        {
            var result = new ApiResponse();

            try
            {
                await _customerService.DeleteCustomer(customerId);
                result.Message = "Customer Deleted";
                result.Status = System.Net.HttpStatusCode.OK;

            }
            catch (Exception e)
            {
                logger.LogError(e.Message);
                result.Message = "Create customer Failed, Something went wrong!, Contact the Administrator to check the log";
                result.Status = System.Net.HttpStatusCode.InternalServerError;
                result.Result = null;
                return result;
            }
            return result;
        }

        public async Task<ApiResponse> EditCustomer(Guid customerId, CustomerDto customerApiModel)
        {
            var result = new ApiResponse();

            try
            {
                var editResult = await _customerService.EditCustomer(customerId, customerApiModel);
                if (editResult > 0)
                {
                    result.Message = "";
                    result.Status = System.Net.HttpStatusCode.OK;
                }
                else
                {

                    result.Message = "Not Found";
                    result.Status = System.Net.HttpStatusCode.NotFound;
                    logger.LogError("Not Found");

                }
            }
            catch (Exception e)
            {
                result.Message = "Update customer Failed, Something went wrong!, Contact the Administrator to check the log";
                result.Status = System.Net.HttpStatusCode.InternalServerError;
                result.Result = null;
                logger.LogError(e.Message);
                return result;
            }
            return result;
        }

        public async Task<ApiResponse> SearchCustomer(string name)
        {
            var result = new ApiResponse();
            if (name != null)
            {
                try
                {
                    var searchResults = await _customerService.SearchCustomer(name);
                    var searchDtos = new List<CustomerDto>();
                    result.Message = "";
                    result.Result = searchResults;
                    result.Status = System.Net.HttpStatusCode.OK;
                }
                catch (Exception e)
                {
                    result.Message = "Search customer Failed, Something went wrong!, Contact the Administrator to check the log";
                    result.Status = System.Net.HttpStatusCode.InternalServerError;
                    result.Result = null;
                    logger.LogError(e.Message);
                    return result;
                }
            }
            else
            {
                logger.LogError("Name is null");
                result.Message = "Name is null";
            }
            return result;
        }    
    }
}
