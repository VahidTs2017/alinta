﻿namespace Alinta.Application.Validation
{
    using Alinta.Application.Model;
    using FluentValidation;
    using System;

    public class CustomerValidator : AbstractValidator<CustomerDto>
    {
        public CustomerValidator()
        {
            RuleFor(c => c.FirstName).NotNull().WithMessage("First Name Could not be null");
            RuleFor(c => c.FirstName).Must(c => c?.Length > 1 && c?.Length < 50).WithMessage("First Name length must be between 1 and 255 ");
            RuleFor(c => c.LastName).NotNull().WithMessage("Last Name Could not be null");
            RuleFor(c => c.LastName).Must(c => c?.Length > 1 && c?.Length < 50).WithMessage("Last Name length must be between 1 and 255 ");
            RuleFor(c => c.DateOfBirth).Must(CheckDate).WithMessage("Customer age must be more than 18");
            RuleFor(c => c.CustomerId)
                .Must(IsValidGuid).WithMessage("Invalid Customer ID");
        }

        private bool CheckDate(DateTime date)
        {
            return DateTime.UtcNow.Date.Year - date.Year >= 18;
        }

        private bool IsValidGuid(Guid customerId)
        {
            return Guid.TryParse(customerId.ToString(), out var guidOut);
        }
    }
}