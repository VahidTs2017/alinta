﻿namespace Alinta.Application.Service
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Alinta.Application.Model;

    public interface ICustomerService
    {
        Task<int> AddCustomer(CustomerDto customerDto);
        Task<int> EditCustomer(Guid customerId, CustomerDto customerDto);
        Task<int> DeleteCustomer(Guid customerId);
        Task<List<CustomerDto>> SearchCustomer(string name);
    }
}
