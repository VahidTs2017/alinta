﻿namespace Alinta.Application.Service
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Alinta.Application.Model;
    using Alinta.Core.Model;
    using Alinta.Data.Repository;

    public class CustomerService : ICustomerService
    {
        private readonly ICustomerDataRepository _customerData;
        public CustomerService(ICustomerDataRepository customerData)
        {
            _customerData = customerData;
        }
        public async Task<int> AddCustomer(CustomerDto customerApiModel)
        {
            var customerDto = ApiModelToDtoTransformation(customerApiModel);
            return await _customerData.AddCustomer(customerDto);
        }

        public async Task<int> DeleteCustomer(Guid customerId)
        {
            return await _customerData.DeleteCustomer(customerId);
        }

        public async Task<int> EditCustomer(Guid customerId, CustomerDto customerDto)
        {
            return await _customerData.EditCustomer(customerId, ApiModelToDtoTransformation(customerDto));
        }

        public async Task<List<CustomerDto>> SearchCustomer(string name)
        {
            var searchResult = await _customerData.SearchCustomer(name);
            var customerDtoList = new List<CustomerDto>();
            foreach (var item in searchResult)
            {
                var dto = CustomerDtoTransformation(item);
                customerDtoList.Add(dto);
            }
            return customerDtoList;
        }

     

        private Customer ApiModelToDtoTransformation(CustomerDto customerDto, Guid? customerId = null)
        {
            var customer = new Customer();           
            customer.FirstName = customerDto?.FirstName;
            customer.LastName = customerDto?.LastName;
            customer.DateOfBirth = customerDto.DateOfBirth;
            return customer;
        }
        private CustomerDto CustomerDtoTransformation(Customer customer)
        {
            var customerDto = new CustomerDto();
            customerDto.CustomerId = customer.CustomerId;
            customerDto.FirstName = customer?.FirstName;
            customerDto.LastName = customer?.LastName;
            customerDto.DateOfBirth = customer.DateOfBirth;
            return customerDto;
        }


    }
}