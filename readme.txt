
Solution Created in 3 layer
-Presentation
-Application
-Core
-Data

Tech used:
-.Net Core 2.2
 Dependency injection 
-Open API and Swagger
-In memory Entity Framework
-Fluent Validation
-Xunit Testing


-4 API End point created for Add/Edit/Delete and Search

Swagger url:
http://localhost:56252/swagger/index.html
