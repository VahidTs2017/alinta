﻿using Alinta.Core.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace Alinta.Data
{
    public class DataGenerator
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new CustomerDataContext(
                serviceProvider.GetRequiredService<DbContextOptions<CustomerDataContext>>()))
            {
                if (context.Customer.Any())
                {
                    return;   
                }

                context.Customer.AddRange(
                    new Customer
                    {
                        CustomerId = new Guid("b7aa2e81-075e-46db-889a-6fc2c4dbef94"),
                        FirstName = "John",
                        LastName = "Smith",
                        DateOfBirth = new DateTime(1995, 5, 3),
                        CreateDateTime = DateTime.UtcNow

                    },
                     new Customer
                     {
                         CustomerId = new Guid("d5c37e73-8762-4d99-a345-6339b1f54f8d"),
                         FirstName = "Matt",
                         LastName = "Roberts",
                         DateOfBirth = new DateTime(1945, 3, 2),
                         CreateDateTime = DateTime.UtcNow

                     },
                      new Customer
                      {
                          CustomerId = new Guid("c858bdbf-5592-4d71-b797-204f3cc36eff"),
                          FirstName = "Tom",
                          LastName = "Ryan",
                          DateOfBirth = new DateTime(1972, 5, 11),
                          CreateDateTime = DateTime.UtcNow
                      }
                   );
                context.SaveChanges();
            }
        }
    }
}