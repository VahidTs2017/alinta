﻿namespace Alinta.Data.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Alinta.Core.Model;

    public interface ICustomerDataRepository
    {
        Task<int> AddCustomer(Customer customer);

        Task<int> EditCustomer(Guid customerId, Customer customer);

        Task<int> DeleteCustomer(Guid customerId);

        Task<List<Customer>> SearchCustomer(string name);
    }
}