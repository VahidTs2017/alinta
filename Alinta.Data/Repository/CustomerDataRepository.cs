﻿namespace Alinta.Data.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Alinta.Core.Model;
    using Microsoft.EntityFrameworkCore;

    public class CustomerDataRepository : ICustomerDataRepository
    {
        private CustomerDataContext _context;
        public CustomerDataRepository(CustomerDataContext customerDataContext)
        {
            _context = customerDataContext;
        }
        public async Task<int> AddCustomer(Customer Customer)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CustomerDataContext>();
            var customer = this.DtotoDataModeTransformation(Customer, Customer.CustomerId);

            customer.CustomerId = new Guid();
            customer.LastModifiedDateTime = DateTime.UtcNow;
            customer.CreateDateTime = DateTime.UtcNow;

            _context.Customer.Add(customer);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> DeleteCustomer(Guid customerId)
        {
            var customer = new Customer() { CustomerId = customerId };

            var optionsBuilder = new DbContextOptionsBuilder<CustomerDataContext>();
            _context.Customer.Remove(customer);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> EditCustomer(Guid customerId, Customer Customer)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CustomerDataContext>();
            if (_context.Customer.Where(c => c.CustomerId.Equals(customerId)).FirstOrDefault() != null)
            {
                var customer = DtotoDataModeTransformation(Customer, Customer.CustomerId);
                customer.CustomerId = customerId;
                customer.LastModifiedDateTime = DateTime.UtcNow;
                _context.Update(customer);
                return await _context.SaveChangesAsync();
            }
            else
            {
                return -1;
            }
          
        }

        public async Task<List<Customer>> SearchCustomer(string name)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CustomerDataContext>();
            return await _context.Customer.Where(c => c.FirstName.Contains(name, StringComparison.InvariantCultureIgnoreCase)
            || c.LastName.Contains(name, StringComparison.InvariantCultureIgnoreCase)).ToListAsync();
        }

        private Customer DtotoDataModeTransformation(Customer Customer, Guid? customerId = null)
        {
            var customer = new Customer();
            customer.CustomerId = Customer.CustomerId;
            customer.FirstName = Customer?.FirstName;
            customer.LastName = Customer?.LastName;
            customer.DateOfBirth = Customer.DateOfBirth;
            return customer;
        }
    }
}