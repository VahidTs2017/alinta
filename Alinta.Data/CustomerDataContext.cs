﻿namespace Alinta.Data
{
    using Alinta.Core.Model;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Infrastructure;

    public class CustomerDataContext : DbContext
    {
        public CustomerDataContext(DbContextOptions<CustomerDataContext> options)
        : base(options)
        {

        }

        public DbSet<Customer> Customer { get; set; }
        public DatabaseFacade GetDatabase()
        {
            return this.Database;
        }
    }
}
