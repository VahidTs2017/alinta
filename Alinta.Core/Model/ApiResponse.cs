﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Alinta.Core.Models
{
    public class ApiResponse
    {
        public HttpStatusCode Status { get; set; }
        public string Message { get; set; }
        public object Result { get; set; }
    }
}
