﻿
namespace Alinta.Presentation
{
    using Alinta.Application;
    using Alinta.Application.Model;
    using Alinta.Application.Service;
    using Alinta.Application.Validation;
    using Alinta.Data;
    using Alinta.Data.Repository;
    using FluentValidation;
    using FluentValidation.AspNetCore;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;

    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(setup => {
            }).AddFluentValidation();
            services.AddTransient<IValidator<CustomerDto>, CustomerValidator>();
            services.AddScoped<ICustomerApplication, CustomerApplication>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<ICustomerDataRepository, CustomerDataRepository>();
            services.AddDbContext<CustomerDataContext>(options => options.UseInMemoryDatabase(databaseName: "Alinta"));


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info { Title = "Alinta" });
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
            public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            loggerFactory.AddLog4Net();

            app.UseMvc();
        }
    }
}
