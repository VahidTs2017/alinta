﻿namespace Alinta.Controller.Presentation
{
    using System;
    using System.Threading.Tasks;
    using Alinta.Application;
    using Alinta.Application.Model;
    using Alinta.Core.Models;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/customer")]
    [ApiController]
    public class CustomerController : ControllerBase
    {

        private readonly ICustomerApplication _CustomerApplication;
        public CustomerController(ICustomerApplication CustomerApplication)
        {
            _CustomerApplication = CustomerApplication;
        }

        [HttpPost]
        //Add
        public async Task<ApiResponse> Add(CustomerDto Customer)
        {
            return await _CustomerApplication.AddCustomer(Customer);
        }

        [HttpPut("{id}")]
        public async Task<ApiResponse> Edit(Guid Id, CustomerDto Customer)
        {
            if (Id != Guid.Empty)
            {
               return await _CustomerApplication.EditCustomer(Id, Customer);
            }
            else
            {
                return new ApiResponse
                {
                    Message = "CustomerId is Empty",
                    Result = null,
                    Status = System.Net.HttpStatusCode.BadRequest
                };
            }
        }

        [HttpGet("{name}")]
        public async Task<ApiResponse> Get(string name)
        {
            return await _CustomerApplication.SearchCustomer(name);
        }


        [HttpDelete("{id}")]
        public async Task<ApiResponse> Delete(Guid id)
        {
            return await _CustomerApplication.DeleteCustomer(id);
        }

    }
}